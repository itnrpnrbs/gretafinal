﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]

public class animdene : MonoBehaviour
{

    public Animator anim;
    public float speed = 6f;
    Vector3 pos;

   
    public Vector3 jump;
    public float jumpForce = 2.0f;
     
    public bool isGrounded;
    Rigidbody rb;
    
    
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        jump = new Vector3(0.0f, 2.0f, 0.0f);
    }

    void OnCollisionStay()
    {
        isGrounded = true;
    }
    
    // Update is called once per frame
    void Update()
    {
        pos = transform.position;
        
        if(Input.GetKeyDown(KeyCode.Space) && isGrounded){
     
            anim.SetBool("jump",true);
            rb.AddForce(jump * jumpForce, ForceMode.Impulse);
            isGrounded = false;
        }

        if (Input.GetMouseButtonDown(0)) 
        {
            
            anim.SetBool("crash", true);
            Saldır();
        }
        if(Input.GetMouseButtonDown(1))
        {
           
            anim.SetBool("repair", true);
            Saldır();
        }
        
        if (Input.GetKey("f"))
        {
            anim.SetBool("push", true);
        }
        if (Input.GetKeyUp("f"))
        {
            anim.SetBool("push", false);
        }
        
        if (Input.GetKey("d"))
        {
            speed = 6f;
            SagYuru();

        }
        if (Input.GetKeyUp("d"))
        {
            speed = 0;
            anim.SetBool("yuru", false);
        }
        if (Input.GetKey("a"))
        {
            speed = 6f;
            SolYuru();

        }
        if (Input.GetKeyUp("a"))
        {
            speed = 0;
            anim.SetBool("yuru", false);
        }

        if (Input.GetKey("c"))
        {
            anim.SetBool("crouch", true);
            if (anim.GetBool("yuru"))
            {
                CokYuru();
            }
        }

        if (Input.GetKeyUp("c"))
        {
            anim.SetBool("crouch", false);

        }

        
        if (Input.GetKey("space"))
        {
            anim.SetBool("jump", true);
            Jump();
        }
        if (Input.GetKeyUp("space"))
        {
            anim.SetBool("jump", false);
        }

        transform.position = pos;


    }

    private void Jump()
    {
        
        
        
    }

    private void Saldır()
    {

        if (anim.GetBool("crash") == true)
        {
            anim.SetBool("crash", false);
            Debug.Log("parçala");
        } 
        if (anim.GetBool("repair") == true)
        {
            anim.SetBool("repair", false);
            Debug.Log("onar");

        } 
    }

    private void SolYuru()
    {
        if (!anim.GetBool("crouch"))
        {
            pos.z += speed * Time.deltaTime;
            anim.SetBool("yuru", true);
            transform.LookAt(transform.position + Vector3.forward);
        }

        if (anim.GetBool("crouch"))
        {
            pos.z += speed * Time.deltaTime;
            anim.SetBool("yuru", true);
            transform.LookAt(transform.position + Vector3.forward);
        }
    }

    private void CokYuru()
    {
        if (Input.GetKey(KeyCode.A))
        {
            SolYuru();
        }

        else
        {
            SagYuru();
        }
    }

    private void SagYuru()
    {


        if (!anim.GetBool("crouch"))
        {
            pos.z -= speed * Time.deltaTime;
            anim.SetBool("yuru", true);
            transform.LookAt(transform.position + Vector3.back);
        }

        if (anim.GetBool("crouch"))
        {
            pos.z -= speed * Time.deltaTime;
            anim.SetBool("yuru", true);
            transform.LookAt(transform.position + Vector3.back);
        }

    }

}

//void CrouchFunction()
//{
//    if (anim.GetBool("crouch") == true)
//    {
//        transform.Translate. =  new Vector3(1, LocalScaleY, 1);
//        ccr_controller.height = ControllerHeight;
//        Debug.Log("c_func 1");
//    }
//    else
//    {
//        t_mesh.localScale = new Vector3(1, 1, 1);
//        ccr_controller.height = 1.8f;
//        Debug.Log("c_func 0");
//    }
//}





