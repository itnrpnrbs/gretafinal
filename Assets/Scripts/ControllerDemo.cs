﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerDemo : MonoBehaviour
{
    public float speed = 6f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.A))
        {
            transform.position += Vector3.forward * (speed * Time.deltaTime);
        }else if (Input.GetKey(KeyCode.D))
        {
            transform.position += Vector3.back * (speed * Time.deltaTime);
        }
    }
}
