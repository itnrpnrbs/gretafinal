﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HighlightScript : MonoBehaviour
{
    public Shader ethanGray; // Highlight Shader'ı
    private float radius;
    public float border;
    private bool effectIsOn;
    private Renderer rend;
    private bool inCoroutine,inCoroutineDeneme;
    public Camera cameratwo,cam;
    public GameObject LightSun;
    private bool deneme;
    // ---------------------
    private void Start()
    {
        cam = GetComponent<Camera>();
    }

    private void Update()
    {
        if (Input.GetButton("Fire2"))
        {
            if(!inCoroutine)
                StartCoroutine(HighlightAll());
        }
    }
    
    

    IEnumerator HighlightAll()
    {
        inCoroutine = true;
        yield return new WaitForSeconds(0.2f);
        cameratwo.depth = -1;// Player kamerası, Highlighted gözükmesi için depth -1 yapılıp efekt sonrası 0 olarak değiştiriyoruz.
        FindAll(this.transform.position,border); // Player etrafındaki bütün colliderları bulup(bordera göre), shaderlarını değiştiriyor.
        cam.SetReplacementShader(ethanGray, "XRay");// //
        if (!inCoroutineDeneme)
            StartCoroutine(LightsOff());
        deneme = false;
        inCoroutine = false;
    }

    IEnumerator LightsOff()
    {
        inCoroutineDeneme = true;
        yield return new WaitForSeconds(1.6f);
        deneme = true;
        inCoroutineDeneme = false;
    }

        
    void FindAll(Vector3 center, float radius)
    {
        Collider[] hitColliders = Physics.OverlapSphere(center, radius);
        var i = 0;
        while (i < hitColliders.Length)
        {    
            if(hitColliders[i].gameObject!=null)
                print(hitColliders[i].gameObject.tag);
            if(hitColliders[i].GetComponent<Renderer>()!=null&&!deneme&& hitColliders[i].gameObject.tag =="Fragile")
                hitColliders[i].GetComponent<Renderer>().material.shader = ethanGray;
            else if(hitColliders[i].GetComponent<Renderer>()!=null&&deneme)
                hitColliders[i].GetComponent<Renderer>().material.shader = Shader.Find("Standard");
            i++;
        }
    }
}
