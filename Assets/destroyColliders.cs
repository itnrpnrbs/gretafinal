﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class destroyColliders : MonoBehaviour
{
    
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(disableColliders());
    }

    private IEnumerator disableColliders()
    {
        
        yield return new WaitForSeconds(0.2f);
        GetComponent<Collider>().enabled = false;
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
