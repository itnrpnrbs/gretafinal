﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pushTrigger : MonoBehaviour
{
    public static bool pushStarts;
    // Start is called before the first frame update
    void Start()
    {
       
    }
    
    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player")&&GetComponentInParent<MeshRenderer>().enabled)
        {
            GetComponentInParent<Rigidbody>().useGravity = true;
            GetComponentInParent<Rigidbody>().isKinematic = false;

        }
    }

    private void OnTriggerStay(Collider other)
    {
        pushStarts = true;
    }

    private void OnTriggerExit(Collider other)
    {
        pushStarts = false;
    }
}
